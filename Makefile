TARGET=PFPL
SRC_DIR=src

all:
	cd $(SRC_DIR); alex Lexer.x; happy Parser.y; ghc --make $(TARGET).hs -outputdir ../output;
	mv $(SRC_DIR)/PFPL PFPL

simple:
	cd $(SRC_DIR); ghc --make $(TARGET).hs -outputdir ../output;
	mv $(SRC_DIR)/PFPL PFPL

doc:
	haddock -o docs -h src/*.hs

lexer:
	alex $(SRC_DIR)/Lexer.x

parser:
	happy $(SRC_DIR)/Parser.y

clean:
	rm -f -R output/
	rm -f PFPL
	rm -f -R docs/

rebuild: clean all

