{
module Lexer where

import Data.Int (Int32)
}

%wrapper "basic"

$digit = 0-9
$alpha = [a-zA-Z]

tokens :-
       $white+ ;
       "//".* ;
       "(" { const TokenLeftPar }
       ")" { const TokenRightPar }
       "{" { const TokenLeftCur }
       "}" { const TokenRightCur }
       "[" { const TokenLeftBra }
       "]" { const TokenRightBra }
       "let" { const TokenLet }
       "const" { const TokenConst }
       "read" { const TokenRead }
       "function" { const TokenFunction }
       "=" { const TokenDecl }
       ":=" { const TokenAffect }
       ";" { const TokenSemiColon }
       "," { const TokenComma }
       "::" { const TokenType }
       "struct" { const TokenStruct }
       "Int" { const TokenTInt }
       "Float" { const TokenTFloat }
       "Bool" { const TokenTBool }
       "Void" { const TokenTVoid }
       "if" { const TokenIf }
       "then" { const TokenThen }
       "else" { const TokenElse }
       "while" { const TokenWhile }
       "case" { const TokenCase}
       "of" { const TokenOf }
       "->" { const TokenArr }
       "_" { const TokenJoker }
       "return" { const TokenReturn }
       "@" { const TokenAt }
       "true" { const TokenTrue }
       "false" { const TokenFalse}
       "+=" { const TokenPlusEq }
       "-=" { const TokenMinusEq }
       "*=" { const TokenTimesEq }
       "/=" { const TokenSlashEq }
       "%=" { const TokenModEq }
       "++" { const TokenInc }
       "--" { const TokenDec }
       "&&" { const TokenAnd }
       "||" { const TokenOr }
       "!" { const TokenNot }
       "==" { const TokenEqual }
       "!=" { const TokenInequal }
       "<" { const TokenLower }
       "<=" { const TokenLowerEq }
       ">" { const TokenGreater }
       ">=" { const TokenGreaterEq }
       "+" { const TokenPlus }
       "-" { const TokenMinus }
       "*" { const TokenTimes }
       "/" { const TokenSlash }
       "%" { const TokenMod }
       "print" { const TokenPrint }
       " -"? $digit+ "." $digit+ { TokenFloat . read }
       " -"? $digit+ {TokenInt . read }
       $alpha [$alpha $digit \_]* { TokenVar }
       "dimension" { TokenDimension }
       "~" { const TokenTilda }
       "^" { const TokenCirconflex }
       "." { const TokenPoint }

{

data Token = 
     TokenLeftPar
   | TokenRightPar	
   | TokenLeftCur
   | TokenRightCur
   | TokenLeftBra
   | TokenRightBra
   | TokenLet
   | TokenConst
   | TokenRead
   | TokenFunction
   | TokenDecl
   | TokenAffect
   | TokenSemiColon
   | TokenComma
   | TokenType
   | TokenStruct
   | TokenTInt
   | TokenTFloat
   | TokenTBool
   | TokenTVoid
   | TokenIf
   | TokenThen
   | TokenElse
   | TokenWhile
   | TokenCase
   | TokenOf
   | TokenArr
   | TokenJoker
   | TokenReturn
   | TokenAt
   | TokenTrue
   | TokenFalse
   | TokenPlusEq
   | TokenMinusEq
   | TokenTimesEq
   | TokenSlashEq
   | TokenModEq
   | TokenInc
   | TokenDec
   | TokenAnd
   | TokenOr
   | TokenNot
   | TokenEqual
   | TokenInequal
   | TokenLower
   | TokenLowerEq
   | TokenGreater
   | TokenGreaterEq
   | TokenPlus
   | TokenMinus
   | TokenTimes
   | TokenSlash
   | TokenMod
   | TokenPrint
   | TokenInt Int32
   | TokenFloat Float
   | TokenBool Bool
   | TokenVar String
   | TokenDimension
   | TokenTilda
   | TokenCirconflex
   | TokenPoint
   deriving (Eq, Show)
	   
lexer = alexScanTokens

}
