{
module Parser where

import qualified Data.Map as M

import qualified Lexer as L
import qualified AST

}

%name parser
%tokentype { L.Token }
%error { parseError }

%token
	'(' { L.TokenLeftPar }
	')' { L.TokenRightPar }
	'{' { L.TokenLeftCur }
	'}' { L.TokenRightCur}
	'[' { L.TokenLeftBra }
	']' { L.TokenRightBra }
	let { L.TokenLet }
	const { L.TokenConst }
	read { L.TokenRead }
	function { L.TokenFunction }
	'=' { L.TokenDecl }
	':=' { L.TokenAffect }
	';' { L.TokenSemiColon }
	',' { L.TokenComma }
	'::' { L.TokenType }
	struct { L.TokenStruct }
	Int { L.TokenTInt }
	Float { L.TokenTFloat }
	Bool { L.TokenTBool }
        Void { L.TokenTVoid }
	if { L.TokenIf }
	then { L.TokenThen }
	else { L.TokenElse }
	while { L.TokenWhile }
	case { L.TokenCase }
	of { L.TokenOf }
	'->' { L.TokenArr }
	'_' { L.TokenJoker }
	return { L.TokenReturn }
	'@' { L.TokenAt }
	true { L.TokenTrue }
	false { L.TokenFalse }
	and { L.TokenAnd }
	or { L.TokenOr }
	not { L.TokenNot }
	eq { L.TokenEqual }
	ineq { L.TokenInequal }
	'<' { L.TokenLower }
	'<=' { L.TokenLowerEq }
	'>' { L.TokenGreater }
	'>=' { L.TokenGreaterEq }
	'+' { L.TokenPlus }
	'-' { L.TokenMinus}
	'*' { L.TokenTimes }
	'/' { L.TokenSlash }
	'%' { L.TokenMod }
	print { L.TokenPrint }
	float { L.TokenFloat $$ }
	int { L.TokenInt $$ }
	var { L.TokenVar $$ }
	'~' { L.TokenTilda }
	'^' { L.TokenCirconflex }
	'.' { L.TokenPoint }

%left return
%left and or
%left eq ineq not
%left '<' '<=' '>' '>='
%left '+' '-' 
%left '*' '/' '%'

%nonassoc '<' '>'
%nonassoc '<' '>='
%nonassoc '>' '<='

%%

Source 
  : function var '(' Args ')' '::' Type '{' Command '}' { AST.Function $2 $4 $7 $9 }
  | struct var '{' FieldsDef '}' { AST.StructDef $2 $4 }
  | Source Source { AST.SSeq $1 $2 }

Args 
  : {- empty -} { [] }
  | Arg { [$1] }
  | Arg ',' Args { $1 : $3 }

Arg 
  : var '::' Type { ($1, $3) }

FieldsDef 
  : FieldDef { [$1] }
  | FieldDef ';' FieldsDef { $1 : $3 }

FieldDef 
  : var '::' Type { ($1, $3) }

ArrayBra 
  : ArrayBraDef { [$1] }
  | ArrayBraDef ArrayBra { $1 : $2 }

ArrayBraDef 
  : '[' Exp ']' { $2 }

Command	
  : Com ';' { $1 }
  | if '(' BExp ')' then '{' Command '}' Else { AST.If $3 $7 $9 }
  | while '(' BExp ')' '{' Command '}' { AST.While $3 $6 }
  | case Exp of '{' Cases '}' { AST.CaseOf $2 $5 }
  | Command Command { AST.Seq $1 $2 }

Com 
  : let const var '=' Exp { AST.ConstDecl $3 $5 }
  | let var '=' Exp { AST.Decl $2 $4 }
  | Exp ':=' Exp { AST.Affect $1 $3 }
  | print Exp { AST.Print $2 }
  | return Return { AST.Return $2 }

Return 
  : {- empty -} { Nothing }
  | Exp { Just $1 }

Else 
  : {- empty -} { Nothing }
  | else '{' Command '}' { Just $3 }

Cases 
  : SingleCase { [$1] }
  | Cases SingleCase { $2 : $1 }

SingleCase 
  : Exp '->' '{' Command '}' ';' { ($1, $4) }
  | '_' '->' '{' Command '}' ';' { (AST.Joker, $4) }

Exp 
  : '(' SubExp ')' { $2 }
  | SubExp { $1 }

SubExp 
  : var { AST.Var $1 }
  | read Type { AST.Read $2 }
  | var '(' ArgCall ')' { AST.App $1 $3 }
  | '@' '(' SubExp ')' { AST.Cast $3 }
  | AExp { AST.A $1 }
  | BExp { AST.B $1 }
  | '{' Fields '}' '::' Type { AST.Struct $2 $5 }
  | Type ArrayBra { AST.Array $2 $1 }
  | var ArrayBra { AST.ArrayElement $1 $2 }

Fields 
  : Field { [$1] }
  | Field ';' Fields { $1 : $3 }

Field 
  : var '=' Exp { ($1, $3) }

ArgCall	
  : {- empty -} { [] }
  | SubExp { [$1] }
  | SubExp ',' ArgCall { $1 : $3 }

AExp 
  : int { AST.CstInt $1 }
  | float { AST.CstFloat $1 }
  | Exp '+' Exp { AST.Plus $1 $3 }
  | Exp '-' Exp { AST.Minus $1 $3 }
  | Exp '*' Exp { AST.Times $1 $3 }
  | Exp '/' Exp { AST.Div $1 $3 }
  | Exp '%' Exp { AST.Mod $1 $3 }

BExp 
  : true { AST.CstTrue }
  | false { AST.CstFalse }
  | Exp and Exp { AST.And $1 $3 }
  | Exp or Exp { AST.Or $1 $3 }
  | not Exp { AST.Not $2 }
  | Exp eq Exp { AST.Equal $1 $3 }
  | Exp ineq Exp { AST.Inequal $1 $3 }
  | Exp '<' Exp { AST.Lower $1 $3 }
  | Exp '<=' Exp { AST.LowerEq $1 $3 }
  | Exp '>' Exp { AST.Greater $1 $3 }
  | Exp '>=' Exp { AST.GreaterEq $1 $3 }

Type 
  : Int { AST.TypInt }
  | Float '~' Dimension { AST.TypFloat $3 }
  | Bool { AST.TypBool }
  | Void { AST.TypVoid }
  | var { AST.TypVar $1 }

Dimension
  : {- empty -} { [] }
  | Unit { [$1] }
  | Unit '.' Dimension { $1 : $3 }

Unit
  : var '^' int { ($1, $3) }

{

parseError _ = error "Parse Error"

}
