-- |This module defines abstract LLVMIR, and provides a code generation
-- function.
module LLVMIR where

import Control.Monad.State
import Control.Monad.Writer
import Control.Monad.Identity
import Data.Monoid
import qualified Data.Map as M
import Data.Int (Int32)
import Data.List

import AST

-- http://www.llvm.org/docs/LangRef.html for documentation

-- |This. Is. Ugly.
showHeader = concat $ intersperse "\n" ["declare i32 @printf(i8*, ...)",  -- über yummy
 "declare i32 @scanf(i8*, ...)\n",
 "@frmt_int = constant [4 x i8] c\"%d\\0A\\00\"",
 "@frmt_double = constant [4 x i8] c\"%f\\0A\\00\"",
 "@frmt_true = constant [6 x i8] c\"true\\0A\\00\"",
 "@frmt_false = constant [7 x i8] c\"false\\0A\\00\"",
 "",
 "@sfrmt_int = constant [3 x i8] c\"%d\\00\"",
 "@sfrmt_double = constant [4 x i8] c\"%lf\\00\"",
 "",
 "define void @print_int(i32 %x) {",
 "  %formatstr = getelementptr [4 x i8]* @frmt_int, i64 0, i64 0",
 "  call i32 (i8*, ...)* @printf(i8* %formatstr, i32 %x)",
 "  ret void",
 "}",
 "",
 "define void @print_double(double %x) {",
 "  %formatstr = getelementptr [4 x i8]* @frmt_double, i64 0, i64 0",
 "  call i32 (i8*, ...)* @printf(i8* %formatstr, double %x)",
 "  ret void",
 "}",
 "",
 "define void @print_bool(i1 %x) {",
 "  br i1 %x, label %IfTrue, label %IfFalse",
 "  IfTrue:",
 "    %formatstr = getelementptr [6 x i8]* @frmt_true, i64 0, i64 0",
 "    call i32 (i8*, ...)* @printf(i8* %formatstr)",
 "    ret void",
 "  IfFalse:",
 "    %formatstr2 = getelementptr [7 x i8]* @frmt_false, i64 0, i64 0",
 "    call i32 (i8*, ...)* @printf(i8* %formatstr2)",
 "    ret void",
 "}",
 "define i32 @int_of_double(double %x) {",
 "  %y = fptosi double %x to i32",
 "  ret i32 %y",
 "}",
 "",
 "define double @double_of_int(i32 %x) {",
 "  %y = sitofp i32 %x to double",
 "  ret double %y",
 "}",
 "",
 "define i32 @read_int() {",
 "  %ptr = alloca i32",
 "  call i32 (i8*, ...)* @scanf(i8* getelementptr inbounds",
 "      ([3 x i8]* @sfrmt_int, i32 0, i32 0), i32* %ptr)",
 "  %res = load i32* %ptr",
 "  ret i32 %res",
 "}",
 "",
 "define double @read_double() {",
 "  %ptr = alloca double",
 "  call i32 (i8*, ...)* @scanf(i8* getelementptr inbounds",
 "    ([4 x i8]* @sfrmt_double, i32 0, i32 0), double* %ptr)",
 "  %res = load double* %ptr",
 "  ret double %res",
 "}",
 "",
 "define i1 @read_bool() {",
 "  %ptr = alloca i32",
 "  call i32 (i8*, ...)* @scanf(i8* getelementptr inbounds",
 "    ([3 x i8]* @sfrmt_int, i32 0, i32 0), i32* %ptr)",
 "",
 "  %content = load i32* %ptr",
 "  %res = icmp ne i32 %content, 0",
 "  ret i1 %res",
 "}",
 "\n\n"]

-- |LLVMIR abstract representation.

data LLVMType = 
  I1 
  | I32 
  | LLVMFloat 
  | LLVMVoid 
  | LLVMArray Int LLVMType
  deriving Eq
           
instance Show LLVMType where
  show I1 = "i1"
  show I32 = "i32"
  show LLVMFloat = "double"
  show LLVMVoid = "void"
  show (LLVMArray dim t) = getArrayRepr dim t

type LLVMId = String

data LLVMCst = 
  ICst Int32 
  | FCst Float 
  | BCst Bool 

instance Show LLVMCst where
  show (ICst x) = show x
  show (FCst x) = show x
  show (BCst True) = "true"
  show (BCst False) = "false"

instance Eq LLVMCst where
  (ICst a) == (ICst b) = a == b
  (FCst a) == (FCst b) = a == b
  (BCst a) == (BCst b) = a == b
  _ == _ = False

data LLVMValue = 
  Id LLVMId 
  | Cst LLVMCst 
  | FunCall LLVMType LLVMId [(LLVMType, LLVMValue)]
  | Load LLVMType LLVMValue
  | Store LLVMType LLVMValue LLVMValue
  | Getelementptr LLVMType LLVMValue [LLVMValue]
  | Allocate LLVMType
  | ArrayAllocate LLVMType LLVMValue
           
instance Show LLVMValue where
  show (Id id) = id
  show (Cst x) = show x
  show (FunCall t id args) = 
    "call " ++ show t ++ " " ++ id ++ "(" ++ 
    (concat . intersperse ", " $ map (\(tArg, vArg) -> show tArg ++ " " ++ show vArg) args)
    ++ ")"
  show (Load t id) = "load " ++ show t ++ "* " ++ show id
  show (Store t v id) =
    "store " ++ show t ++ " " ++ show v ++ ", " ++ show t ++ "* " ++ show id
  show (Getelementptr t ptr exps) =
    let e = map (\x -> ", i32 " ++ show x) exps in
    "getelementptr " ++ show t ++ "* " ++ show ptr ++ ", i32 0" ++ foldl (++) "" e
  show (Allocate t) = "alloca " ++ show t
  show (ArrayAllocate t v) = "alloca " ++ show t ++ ", i32 " ++ show v

instance Eq LLVMValue where
  (Id id1) == (Id id2) = id1 == id2
  (Cst x) == (Cst y) = x == y
  _ == _ = False

data LLVMOp = 
  Add | FAdd 
  | Sub | FSub
  | Mul | FMul
  | SDiv | FDiv
  | SRem | FRem
  | LLVMOr | LLVMAnd
  | LLVMXor

instance Show LLVMOp where
  show Add = "add"
  show FAdd = "fadd"
  show Sub = "sub"
  show FSub = "fsub"
  show Mul = "mul"
  show FMul = "fmul"
  show SDiv = "sdiv"
  show FDiv = "fdiv"
  show SRem = "srem"
  show FRem = "frem"
  show LLVMOr = "or"
  show LLVMAnd = "and"
  show LLVMXor = "xor"
                     
data LLVMComp = 
  Eq | Ne | Sgt | Sge | Slt | Sle 
  | Ueq | Une | Ugt | Uge | Ult | Ule 
  
instance Show LLVMComp where
  show Eq = "eq"
  show Ne = "ne"
  show Sgt = "sgt"
  show Sge = "sge"
  show Slt = "slt"
  show Sle = "sle"
  show Ueq = "ueq"
  show Une = "une"
  show Ugt = "ugt"
  show Uge = "uge"
  show Ult = "ult"
  show Ule = "ule"

data LLVMTerm = 
  Ret (Maybe (LLVMType, LLVMValue)) -- Nothing = void    
  | CondBr LLVMValue LLVMId LLVMId
  | Br LLVMId
  | Switch LLVMType LLVMId LLVMId [(LLVMValue, LLVMId)]
    
instance Show LLVMTerm where
  show (Ret x) = "ret " ++ case x of
    Nothing -> "void"
    Just (t, v) -> show t ++ " " ++ show v
  show (CondBr v l1 l2) = 
    "br " ++ show I1 ++ " " ++ show v ++ ", label %L" ++ l1 ++ ", label %L" ++ l2
  show (Br l) = "br label %L" ++ l
  show _ = ""

data LLVMInstr = 
  AssignOp LLVMValue LLVMOp LLVMType LLVMValue LLVMValue
  | Assign LLVMValue LLVMValue
  | Comp LLVMValue LLVMComp LLVMType LLVMValue LLVMValue
  | Label LLVMId
  | Term LLVMTerm
  | Simple LLVMValue
    
instance Show LLVMInstr where
  show (AssignOp v op t v1 v2) = 
    show v ++ " = " ++ show op ++ " " ++ 
    show t ++ " " ++ show v1 ++ ", " ++ show v2
  show (Assign v v') = case v' of
    FunCall _ _ _ -> show v ++ " = " ++ show v'
    _ -> show v ++ " = " ++ show v'
  show (Comp v comp t v1 v2) = 
    show v ++ " = " ++ (case t of I32 -> "icmp "; _ -> "fcmp ") ++ show comp ++ " " ++
    show t ++ " " ++ show v1 ++ ", " ++ show v2
  show (Label id) = "L" ++ id ++ ":"
  show (Term t) = show t
  show (Simple v) = show v

data LLVMFun =
  Header
  | LLVMFun LLVMType LLVMId [(LLVMType, LLVMValue)] [LLVMInstr]

instance Show LLVMFun where
  show Header = showHeader
  show (LLVMFun t id args body) =
    "define " ++ show t ++ " @" ++ id ++ "(" ++
    (concat . intersperse ", " . map (\(typ, arg) -> show typ ++ " " ++ show arg) $ args)
    ++ ") {\n" ++ (concat . intersperse "\n" . map show $ body) ++ "\n}\n"


-- |LLVM IR generation

getVar s = Id . (s ++) . show

getIntVar :: Int -> LLVMValue
getIntVar = getVar "%t"

getStrVar :: String -> LLVMValue
getStrVar = Id . ("%" ++)

getStrIntVar :: Int -> String
getStrIntVar = ("t" ++) . show

getLabel :: Int -> LLVMId
getLabel = show
      
getLLVMType :: Type -> LLVMType
getLLVMType t = case t of
  TypInt -> I32
  TypFloat _ -> LLVMFloat
  TypBool -> I1
  TypVoid -> LLVMVoid
  TypArray dim t -> LLVMArray dim (getLLVMType t)
  _ -> error "Type conversion error."

getType = getLLVMType . getType'
  where getType' (TVar _ t) = t
        getType' (TConstVar _ t) = t
        getType' (TArrayElement _ _ t) = t
        getType' (TArray _ t) = t
        getType' (TA _ t) = t
        getType' (TB _ t) = t
        getType' (TApp _ _ t) = t
        getType' (TCast _ t) = t
        getType' (TRead t) = t
        getType' TJoker = TypJoker

getArrayRepr :: Int -> LLVMType -> String
getArrayRepr 0 t = show t
getArrayRepr dim t = "[0 x " ++ (getArrayRepr (dim - 1) t) ++ "]"

genArrayType :: Type -> [TExp] -> LLVMType
genArrayType (TypArray dim typ) exps = case dim - (length exps) of
          0 -> getLLVMType typ
          n -> getLLVMType (TypArray n typ)
genArrayType t _ = getLLVMType t

type Gen a = (Int, a)

-- |A little DSL, to make generation easier.

(#) v = getIntVar v

(§) = getLabel

(<~) x y = Assign ((#) x) ((#) y)
infix 0 <~

(<~~) x y = Assign ((#) x) y
infix 0 <~~

(<<) x y = Assign x y
infix 0 <<

-- |AST generation

genAST (TFunction f l t body) = [LLVMFun t' f args bod]
  where t' = getLLVMType t
        args = map (\(a, typ) -> (getLLVMType typ, getStrVar a)) l
        bod = snd . runIdentity $ genCommand 0 body
genAST (TSSeq ast1 ast2) = genAST ast1 ++ genAST ast2

-- |Command language generation

genCommand :: Int -> TCommand -> Identity (Int, [LLVMInstr])
genCommand i (TDecl x (TArray exps t)) =
  let (d, typ) = dim_typ t in
  if d == 1 then do
    (i', e) <- genExp i (head exps)
    return (i', e ++ [getStrVar x << ArrayAllocate (getLLVMType t) ((#) (i' - 1))])
  else do
      (i0, e0) <- genExp i (head exps)
      (_, e') <- foo i0
      (i1, e1) <- genCommand i0 (TDecl (getStrIntVar i0) (TA (TCstInt 0) TypInt))
      (i2, e2) <- genCommand i1 (TWhile (TLower (TVar (getStrIntVar i0) TypInt) (head exps))
                                 (TSeq (TArrayAffect x (getStrIntVar i0) (TArray (tail exps) (dec_array t)) d t)
                                  (TAffect (TVar (getStrIntVar i0) TypInt) 
                                   (TA (TPlus (TVar (getStrIntVar i0) TypInt) (TA (TCstInt 1) TypInt)) TypInt))))
      return (i2, e0 ++ e' ++ e1 ++ e2)
  where foo i = return (i, [getStrVar x << ArrayAllocate (getLLVMType t) ((#) (i - 1))])
        dim_typ (TypArray d typ) = (d, typ)
        dec_array (TypArray d_ t_) = TypArray (d_ - 1) t_
        
genCommand i (TDecl x e) = do
  (i', e') <- genExp i e
  return (i', e' ++
                [getStrVar x << (Allocate $ getType e),
                 Simple $ Store (getType e) ((#) (i' - 1)) (getStrVar x)])
genCommand i (TConstDecl x e) = do
  (i', e') <- genExp i e
  return (i', e' ++ [getStrVar x << (#) (i' - 1)])
genCommand i (TAffect (TArrayElement x exps t) e) = do
  (i', e') <- genExp i (TArrayElement x exps t)
  (i'', e'') <- genExp i' e
  return (i'', e' ++ e'' ++ [Simple $ Store (genArrayType t exps) ((#) (i'' - 1)) ((#) (i' - 2))])
genCommand i (TAffect (TVar x t) e) = do
  (i', e') <- genExp i e
  return (i', e' ++ [Simple $ Store (getLLVMType t) ((#) (i' - 1)) (getStrVar x)])
genCommand i (TArrayAffect x index arr dim (TypArray d t)) =  
  let (i0, e0) = (i + 3, 
                  [i + 1 <~~ Load I32 (getStrVar index),
                   i + 2 <~~ Getelementptr (getLLVMType (TypArray d t)) (getStrVar x) [(#) (i + 1)]]) in
  do
    (i1, e1) <- genCommand (i0 + 1) (TDecl (getStrIntVar i0) arr)
    let (i2, e2) = (i1 + 1, [i1 <~~ Load (getLLVMType (TypArray (dim - 1) t)) (getIntVar i0),
                          Simple $ Store (getLLVMType (TypArray (dim - 1) t)) (getIntVar i1) (getIntVar (i + 2))])
    return (i2, e0 ++ e1 ++ e2)
genCommand i (TIf b body els) = do
  (i', e) <- genBExp i b
  (i1, e1) <- genCommand i' body
  (i2, e2) <- case els of
    Nothing -> return (i1, [])
    Just elseBody -> genCommand i1 elseBody
  return (i2 + 3, e ++ [Term $ CondBr ((#) (i' - 1)) ((§) i2) ((§) (i2 + 1))] 
           ++ [Label $ (§) i2] ++ e1 ++ [Term . Br $ (§) (i2 + 2)]
           ++ [Label $ (§) (i2 + 1)] ++ e2 ++ [Term . Br $ (§) (i2 + 2)]
           ++ [Label $ (§) (i2 + 2)])
genCommand i (TWhile b body) = do
  (i', e') <- genBExp i b
  (i'', e'') <- genCommand i' body
  return (i'' + 3, [Term . Br $ (§) i'', Label $ (§) i''] ++ e'
           ++ [Term $ CondBr ((#) (i' - 1)) ((§) (i'' + 1)) ((§) (i'' + 2))]
           ++ [Label $ (§) (i'' + 1)] ++ e'' ++ [Term . Br $ (§) i'']
           ++ [Label $ (§) (i'' + 2)])
genCommand i (TReturn (Just e)) = do
  (i', e') <- genExp i e
  return (i', e' ++ [Term . Ret $ Just (getType e, getIntVar (i' - 1))])
genCommand i (TReturn Nothing) =
  return (i, [Term $ Ret Nothing])
genCommand i (TPrint e) = do
  (i', e') <- genExp i e
  case getType_ e of
    I1 -> return (i', e' ++ [Simple $ FunCall LLVMVoid "@print_bool" [(I1, (#) (i' - 1))]])
    I32 -> return (i', e' ++ [Simple $ FunCall LLVMVoid "@print_int" [(I32, (#) (i' - 1))]])
    LLVMFloat -> return (i', e' ++ [Simple $ FunCall LLVMVoid "@print_double" [(LLVMFloat, (#) (i' - 1))]])
  where getType_ (TArrayElement _ _ (TypArray _ t)) = getLLVMType t
        getType_ x = getType x
genCommand i (TSeq c1 c2) = do
  (i', c') <- genCommand i c1
  (i'', c'') <- genCommand i' c2
  return (i'', c' ++ c'')

-- |Expression generation

genExp :: Int -> TExp -> Identity (Int, [LLVMInstr])
genExp i (TVar x t) = return (i + 1, [i <~~ Load (getLLVMType t) (getStrVar x)])
genExp i (TConstVar x _) = return (i + 1, [i <~~ getStrVar x])
genExp i (TArrayElement x exps t) = do
  let (i', exps_, exps') = genExps i exps [] []
  return (i' + 2, exps_ ++ [i' <~~ Getelementptr (getLLVMType t) (getStrVar x) exps',
                            i' + 1 <~~ Load (genArrayType t exps) ((#) i')])
  where genExps i [] accExp acc = (i, reverse accExp, reverse acc)
        genExps i (e:es) accExp acc = let (i', e') = runIdentity $ genExp i e in
          genExps i' es (reverse e' ++ accExp) (getIntVar (i' - 1) : acc)
genExp i (TA e t) = do
  return . runIdentity $ genAExp i e t
genExp i (TB e _) = do
  return . runIdentity $ genBExp i e
genExp i (TApp f args t) = do
  let (i', args_, args') = genArgs i args [] []
  return (i' + 1, args_ ++ [i' <~~ FunCall (getLLVMType t) ("@" ++ f) args'])
  where genArgs i [] accExp acc = (i, reverse accExp, reverse acc)
        genArgs i (x:xs) accExp acc = let (i', e') = runIdentity $  genExp i x in
          genArgs i' xs (reverse e' ++ accExp) ((getType x, getIntVar (i' - 1)) : acc)
genExp i (TCast e t) = do
  (i', e') <- genExp i e
  case t of
    TypInt -> return (i' + 1, e' ++ [i' <~~ FunCall I32 "@int_of_double" [(LLVMFloat, (#) (i' - 1))]])
    TypFloat _ -> return (i' + 1, e' ++ [i' <~~ FunCall LLVMFloat "@double_of_int" [(I32, (#) (i' - 1))]])
genExp i (TRead t) = do
  case t of
    TypInt -> return (i + 1, [i <~~ FunCall I32 "@read_int" []])
    TypFloat _ -> return (i + 1, [i <~~ FunCall LLVMFloat "@read_double" []])
    TypBool -> return (i + 1, [i <~~ FunCall I1 "@read_bool" []])

-- |Arithmetical expression generation

genAExp :: Int -> TAExp -> Type -> Identity (Int, [LLVMInstr])
genAExp i (TCstInt x) _ = return (i + 1, [i <~~ (Cst $ ICst x)])
genAExp i (TCstFloat x) _ = return (i + 1, [i <~~ (Cst $ FCst x)])
genAExp i e t =
  let getOp op1 op2 = if t == TypInt then op1 else op2 in
  case e of
    TPlus e1 e2 -> genAExp' i e1 e2 (getOp Add FAdd)
    TMinus e1 e2 -> genAExp' i e1 e2 (getOp Sub FSub)
    TTimes e1 e2 -> genAExp' i e1 e2 (getOp Mul FMul)
    TDiv e1 e2 -> genAExp' i e1 e2 (getOp SDiv FDiv)
    TMod e1 e2 -> genAExp' i e1 e2 (getOp SRem FRem)
  where genAExp' i e1 e2 op = do
          (i', e') <- genExp i e1
          (i'', e'') <- genExp i' e2
          return (i'' + 1,
                  e' ++ e'' ++ [AssignOp ((#) i'') op (getLLVMType t) ((#)(i' - 1)) ((#)(i'' - 1))])

-- |Boolean expression generation

genBExp :: Int -> TBExp -> Identity (Int, [LLVMInstr])
genBExp i TCstTrue = return (i + 1, [i <~~ (Cst $ BCst True)])
genBExp i TCstFalse = return (i + 1, [i <~~ (Cst $ BCst False)])
genBExp i (TNot e) = do
  (i', e') <- genExp i e
  return (i' + 1,
          e' ++ [AssignOp ((#)i') LLVMXor I1 ((#)(i' - 1)) (Cst $ BCst True)])
genBExp i (TAnd e1 e2) = do
  (i', e') <- genExp i e1
  (i'', e'') <- genExp i' e2
  return (i'' + 1,
          e' ++ e'' ++ [AssignOp ((#) i'') LLVMAnd I1 ((#)(i' - 1)) ((#)(i'' - 1))])
genBExp i (TOr e1 e2) = do
  (i', e') <- genExp i e1
  (i'', e'') <- genExp i' e2
  return (i'' + 1,
          e' ++ e'' ++ [AssignOp ((#) i'') LLVMOr I1 ((#)(i' - 1)) ((#)(i'' - 1))])
genBExp i e =
  let getOp e op1 op2 =
        let t = getType e in
        (if t == I32 then op1 else op2, t) in
  case e of
    TEqual e1 e2 -> genBExp' i e1 e2 (getOp e1 Eq Ueq)
    TInequal e1 e2 -> genBExp' i e1 e2 (getOp e1 Ne Une)
    TLower e1 e2 -> genBExp' i e1 e2 (getOp e1 Slt Ult)
    TLowerEq e1 e2 -> genBExp' i e1 e2 (getOp e1 Sle Ule)
    TGreater e1 e2 -> genBExp' i e1 e2 (getOp e1 Sgt Ugt)
    TGreaterEq e1 e2 -> genBExp' i e1 e2 (getOp e1 Sge Uge)
  where genBExp' i e1 e2 (op, typ) = do
          (i', e') <- genExp i e1
          (i'', e'') <- genExp i' e2
          return (i'' + 1,
                  e' ++ e'' ++ [Comp ((#) i'') op typ ((#)(i' - 1)) ((#)(i'' - 1))])

-- |First optimisation Pass

replace x y a = case a of
  AssignOp v op t v1 v2 ->
    if v1 == x then AssignOp v op t y v2
    else if v2 == x then AssignOp v op t v1 y
         else AssignOp v op t v1 v2
  Assign v (FunCall a b c) -> Assign v (FunCall a b (replaceFun x y c))
  Assign v (ArrayAllocate t a) -> Assign v (ArrayAllocate t (if a == x then y else a))
  Assign v (Getelementptr t ptr l) -> Assign v (Getelementptr t ptr (replaceGEP x y l))
  Assign v1 v2 -> 
    if v2 == x then Assign v1 y
    else Assign v1 v2
  Simple (FunCall a b c) -> Simple (FunCall a b (replaceFun x y c))
  Simple (Store a b c) -> Simple (Store a (if b == x then y else b)  c)
  Simple v -> Simple v
  Comp v c t v1 v2 ->
    if v1 == x then Comp v c t y v2
    else if v2 == x then Comp v c t v1 y
         else Comp v c t v1 v2
  Term foo -> case foo of
    Ret r -> case r of
      Just (t, v) -> 
        if v == x then Term . Ret . Just $ (t, y)
        else Term . Ret . Just $ (t, v)
      Nothing -> Term . Ret $ Nothing
    CondBr foo id1 id2 -> 
      if foo == x then Term $ CondBr y id1 id2
      else Term $ CondBr foo id1 id2
    Br foo -> Term $ Br foo
    Switch t id1 id2 l ->
      Term $ Switch t id1 id2 (replaceSwitch x y l)
  Label id -> Label id

replaceFun x y l = map (\(t, v) -> if v == x then (t, y) else (t, v)) l
replaceGEP x y l = map (\v -> if v == x then y else v) l
replaceSwitch x y l = map (\(v, id) -> if v == x then (y, id) else (v, id)) l

postPass :: [LLVMInstr] -> [LLVMInstr]
postPass [] = []
postPass ((Assign x (FunCall a b c)):xs) = (Assign x (FunCall a b c)) : postPass xs
postPass ((Assign x (Load t id)):xs) = (Assign x (Load t id)) : postPass xs
postPass ((Assign x (Allocate t)):xs) = (Assign x (Allocate t)) : postPass xs
postPass ((Assign x (ArrayAllocate t v)):xs) = (Assign x (ArrayAllocate t v)) : postPass xs
postPass ((Assign x (Getelementptr a b c)):xs) = (Assign x (Getelementptr a b c)) : postPass xs
postPass ((Assign x y):xs) = postPass (map (replace x y) xs)
postPass (x:xs) = x : postPass xs

postPassFun l = Header : map (\(LLVMFun a b c d) -> (LLVMFun a b c (postPass . labelRemove $ d))) l

labelRemove :: [LLVMInstr] -> [LLVMInstr]
labelRemove l = case last l of
    (Label id) -> labelRemove $ remove l id
    _ -> l
  where remove [] _ = []
        remove (x:[]) _ = [] -- Last label
        remove (x:xs) id = case x of
          Term t -> case t of
            Br i -> if i == id then remove xs id 
                    else (Term . Br $ i) : remove xs id
            foo -> (Term foo) : remove xs id
          foo -> foo : remove xs id

