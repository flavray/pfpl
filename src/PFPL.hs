import System.Environment
import System.IO
import System.Console.GetOpt
import Data.Monoid

import Lexer
import Parser
import AST
import Checker
import LLVMIR

-- Little redefinition of the Writer monad

newtype Writer w a = Writer { runWriter :: (a, w) }

instance (Monoid w) => Monad (Writer w) where
  return x = Writer (x, mempty)
  (Writer (x,v)) >>= f = 
    let (Writer (y, v')) = f x in 
    Writer (y, v `mappend` v')

tell :: String -> Writer String ()
tell s = Writer ((), s)

-- Command line arguments management

data Flag = Version 
          | Output String 
          | Lexer 
          | Parser 
          | Checker
          | IntermediateCode
          deriving (Show, Eq)

options :: [OptDescr Flag]
options = [ Option ['v'] ["version"] (NoArg Version) 
            "Shows version number"
          , Option ['o'] ["output"] (ReqArg Output "FILE") 
            "Set a file where the result will be written."
          , Option ['l'] ["lexer"] (NoArg Lexer)
            "Prints the result of the lexical analysis (the Token flux)."
          , Option ['p'] ["parser"] (NoArg Parser)
            "Prints the result of the syntaxic analysis (the AST)."
          , Option ['c'] ["checker"] (NoArg Checker)
            "Prints the result of the static-checking (the annotated AST)."
          , Option ['i'] ["intermediate"] (NoArg IntermediateCode)
            "Prints the intermediate code."
          ]

header = "Usage: ./PFPL sourcefile [OPTION...]"

version = "---- PFPL compiler version " ++ show vers ++ " ----"
  where vers = 1.6

-- Output management

type Steps = ([Token], Ast, TAst, String)

outputW :: [Flag] -> Steps -> Writer String ()
outputW flags (tokens, ast, checkedAst, intermediateCode) = do
  tell $ if any (== Version) flags then version ++ "\n\n" else "" 
  tell $ f Lexer tokens
  tell $ f Parser ast
  tell $ f Checker checkedAst
  tell $ if any (== IntermediateCode) flags then intermediateCode ++ "\n\n"
         else ""
    where f flag s = if any (== flag) flags then show s ++ "\n\n" else ""
          
outputFinal :: [Flag] -> String -> IO ()
outputFinal flags outputCode =
  case searchOutput flags of
    Nothing -> putStrLn outputCode
    Just path -> do
      writeFile path outputCode
  where searchOutput [] = Nothing
        searchOutput (Output p:_) = Just p
        searchOutput (_:xs) = searchOutput xs

main :: IO ()
main = do
  (inputPath:args) <- getArgs
  
  let (flags, nonOpts, msgs) = getOpt Permute options args
  
  case (flags, nonOpts, msgs) of
    (flags, [], []) -> return ()
    (_, nonOpts, []) -> 
      error $ "Unrecognized arguments: " ++ unwords nonOpts
    (_, _, msgs) -> error $ concat msgs ++ usageInfo header options
                    
  source <- readFile inputPath
            
  let tokens = lexer source
      ast = parser tokens
      checkedAst = semanticAnalysis ast
      intermediateCode = concatMap show . postPassFun . genAST $ checkedAst
      
  let w = outputW flags (tokens, ast, checkedAst, intermediateCode)
      result = snd $ runWriter w
            
  outputFinal flags result

    
    