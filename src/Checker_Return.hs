module Checker_Return (checkReturn) where

import AST

checkReturn :: TAst -> Bool
checkReturn (TFunction name _ _ body)
  | checkCommand body = True
  | otherwise = error $ "No accessible return in function " ++ name ++ "."
checkReturn (TSSeq a b) = checkReturn a && checkReturn b

checkCommand :: TCommand -> Bool
checkCommand (TReturn _) = True
checkCommand (TIf _ body elseClause) = checkCommand body && check elseClause
  where check clause = case clause of
          Just body -> checkCommand body
          Nothing -> True
checkCommand (TWhile _ body) = checkCommand body
checkCommand (TCaseOf _ l) = and $ map (\(_, x) -> checkCommand x) l
checkCommand (TSeq a b) = checkCommand a || checkCommand b
checkCommand _ = False
