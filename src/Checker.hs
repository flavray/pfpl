{-# LANGUAGE MultiParamTypeClasses #-}

-- |This module is used to make the semantic analysis pass.
-- We take a fully untyped AST, and we typecheck it, and make a few
-- checks on the existence of variables in the differents scopes.
-- The result is a typed and correct AST.
module Checker (semanticAnalysis) where

import qualified Data.Map as M
import Data.List

import AST
import Checker_Return

type Env = M.Map String Type
type VarEnv = M.Map String (Type, Int, Bool) -- Type, Dimension (0 if not array), Constant
type FunctionName = String

-- | Semantic analysis : returns the typed AST.
semanticAnalysis ast =
  let a = semanticAnalysis' ast (M.fromList $ getFunctions ast) in
  if checkReturn a then a
  else a

semanticAnalysis' ast functions = 
  case ast of
    Function f args returnT body -> 
      TFunction f args returnT (snd checkedBody) 
      where checkedBody = typeCheck body f 
                          (M.fromList $ map (\(x, t) -> (x, (t, 0, True))) args) functions
    SSeq a b -> 
      TSSeq (semanticAnalysis' a functions) (semanticAnalysis' b functions)

-- | Generates the list of the functions in the source file, 
-- with their types.
getFunctions :: Ast -> [(FunctionName, Type)]
getFunctions (SSeq a b) = getFunctions a ++ getFunctions b
getFunctions (Function f args returnT body) =
  if diff (map fst args) && isReturnType returnT then
    [(f, TypFun (map snd args) returnT)]
  else error $ "Parameters or return type of " ++ f ++ " are incorrect."
  where diff l = l == nub l

ret = (,)

error' f m = error $ "In function '" ++ f ++ "': " ++ m

getDimension :: Exp -> String -> VarEnv -> Int
getDimension (Array e _) f env = length e
getDimension (ArrayElement v e) f env =
  case M.lookup v env of
    Just (_, dim, _) -> dim - length e
    Nothing -> error' f $ "Unknown array " ++ v
getDimension _ f env = 0

-- | Checks an entire command.
typeCheck :: Command -> String -> VarEnv -> Env -> (VarEnv, TCommand)
typeCheck (Decl v e) f env fun = 
  case M.lookup v fun of
    Just _ -> error' f $ "A function is already called " ++ v ++ "."
    Nothing -> case M.lookup v env of
      Just _ -> error' f $ v ++ " is already binded."
      Nothing -> ret env' (TDecl v e') 
        where e' = typeCheckE e f env fun
              t = typeExp e'
              env' = M.insert v (t, getDimension e f env, False) env
typeCheck (ConstDecl v e) f env fun =
  case M.lookup v fun of
    Just _ -> error' f $ "A function is already called " ++ v ++ "."
    Nothing -> case M.lookup v env of
      Just _ -> error' f $ v ++ " is already binded."
      Nothing -> ret env' (TConstDecl v e') 
        where e' = typeCheckE e f env fun
              t = typeExp e'
              env' = M.insert v (t, getDimension e f env, True) env
typeCheck (Affect x e) f env fun = 
  case M.lookup v fun of
    Just _ -> error' f $ "A function is already called " ++ v ++ "."
    Nothing -> case M.lookup v env of
      Nothing -> error' f $ v ++ " has to be defined first."
      Just (t, dim, const) -> 
        if not const then
          if genType t == t' then
            if getDimension x f env == getDimension e f env then
              ret env (TAffect x' e')
            else error' f $ v ++ " : array dimension error."
          else error' f $ "New binding for " ++ v ++ " has the wrong type."
        else error' f $ v ++ " is defined as a constant, cannot redefine it."
  where e' = typeCheckE e f env fun
        t' = typeExp e'
        goodType (Var s) = s
        goodType (ArrayElement s _) = s
        goodType _ = error' f $ "Bad type of expression."
        v = goodType x
        x' = typeCheckE x f env fun
        genType (TypArray _ t_) = t_
        genType t_ = t_
typeCheck (If b body elseClause) f env fun = 
  ret env $ TIf (typeCheckE b f env fun) (snd $ typeCheck body f env fun) 
  (elseClause >>= \x -> Just . snd $ typeCheck x f env fun)
typeCheck (While b body) f env fun = ret env (TWhile checkedB checkedBody)
  where checkedB = typeCheckE b f env fun
        (_, checkedBody) = typeCheck body f env fun  
typeCheck (CaseOf e l) f env fun = ret env (TCaseOf e' ll)
  where e' = typeCheckE e f env fun
        ll = map treat l
        treat (clause, body) = let clause' = typeCheckE clause f env fun in
          if typeExp clause' == typeExp e' then 
            (clause', snd $ typeCheck body f env fun)
          else error' f $ "Clause " ++ show clause ++ "has the wrong type."
typeCheck (Print e) f env fun = ret env (TPrint $ typeCheckE e f env fun)
typeCheck (Seq a b) f env fun = ret env'' (TSeq a' b')
  where (env', a') = typeCheck a f env fun
        (env'', b') = typeCheck b f env' fun
typeCheck (Return ret) f env fun =
  let t = typeFun $ fun M.! f in
  case ret of
    Just e -> let checkedExp = typeCheckE e f env fun in
      if typeExp checkedExp == t then
        (env, TReturn (Just checkedExp))
      else
        error' f $ "Wrong type is returned, got " ++ (show $ typeExp checkedExp)
        ++ ", expected " ++ (show t) ++ "."
    Nothing ->
      if t == TypVoid then
        (env, TReturn Nothing)
      else
        error' f $ "Wrong type is returned, got Void, expected "
        ++ (show t) ++ "."
  where typeFun (TypFun _ t) = t

-- | Returns the type of a typed expression.
typeExp (TVar _ t) = t
typeExp (TConstVar _ t) = t
typeExp (TArrayElement _ _ t) = t
typeExp (TArray _ t) = t
typeExp (TCast _ t) = t
typeExp (TRead t) = t
typeExp (TA _ t) = t
typeExp (TB _ t) = t
typeExp (TApp _ _ t) = t
typeExp TJoker = TypJoker

-- | Checks if t1 and t2 are "comparable"
eqType t1 t2 = t1 == t2 && t1 /= TypJoker

check e1 e2 f env fun types comp constr =
  if typeExp e1' `comp` typeExp e2' then
    if typeExp e1' `elem` types then do
       constr e1' e2'
    else
      error' f $ "Type error, expression's type must be either "
      ++ (showTypes types) ++ ", got " ++ (show $ typeExp e1') ++ "."
  else error' f $ "Type error, expressions must have the same type, got "
       ++ (show $ typeExp e1') ++ " and " ++ (show $ typeExp e2') ++ "."
  where e1' = typeCheckE e1 f env fun
        e2' = typeCheckE e2 f env fun
        showTypes [] = ""
        showTypes (x:[]) = show x
        showTypes (x:xs) = show x ++ "; " ++ showTypes xs
        
        
-- | Main typing class for expressions
class TypeableExp u t where
  typeCheckE :: u -> String -> VarEnv -> Env -> t
  
instance TypeableExp Exp TExp where
  typeCheckE (Var v) f env fun = 
    case M.lookup v env of
      Just (t, _, const) ->
        if const then TConstVar v t
        else TVar v t
      Nothing -> case M.lookup v fun of
        Just t -> TVar v t
        Nothing -> error' f $ "Unknown variable " ++ v ++ "."
  
  typeCheckE (ArrayElement v e) f env fun =
    let typed_e = map (\x -> typeCheckE x f env fun) e in
    if typ typed_e then
      case M.lookup v env of
        Just (t, dim, _) ->
          TArrayElement v typed_e t
        Nothing -> error' f $ "Unknown array " ++ v ++ "."
    else
      error' f $ "Array indices must be integer."
    where typ [] = True
          typ (x : xs) = typeExp x == TypInt && typ xs
  
  typeCheckE (Array e t) f env fun =
    let typed_e = map (\x -> typeCheckE x f env fun) e in
    if typ typed_e then
      TArray typed_e (TypArray (length e) t)
    else
      error' f $ "Array size must be integer."
    where typ [] = True
          typ (x : xs) = typeExp x == TypInt && typ xs
  
  typeCheckE (Cast e) f env fun =
    let checkedExp = typeCheckE e f env fun in
    case typeExp checkedExp of
      TypInt -> TCast checkedExp (TypFloat [])
      TypFloat d -> TCast checkedExp TypInt
      t -> error' f $ "Could not cast expression, type must be either Int or Float, got " ++ show t ++ "."
  
  typeCheckE (Read t) f env fun =
    case t of 
      TypInt -> TRead t
      TypFloat _ -> TRead t
      TypBool -> TRead t
      _ -> error' f $ "Cannot use read primitive with type " ++ show t
  
  typeCheckE (A aexp) f env fun = case a of
    TCstInt _ -> TA a TypInt
    TCstFloat _ -> TA a (TypFloat [])
    TPlus x _ -> TA a (typeExp x)
    TMinus x _ -> TA a (typeExp x)
    TTimes x _ -> TA a (typeExp x)
    TDiv x _ -> TA a (typeExp x)
    TMod x _ ->  TA a (typeExp x)
    where a = typeCheckE aexp f env fun
          
  typeCheckE (B bexp) f env fun = TB (typeCheckE bexp f env fun) TypBool
  
  typeCheckE (App fct args) f env fun =
    case M.lookup fct fun of
      Nothing -> error' f $ "Unknown function " ++ fct ++ "."
      Just (TypFun argsT returnT) ->
        let checkedArgs = map (\x -> typeCheckE x f env fun) args in 
        if map typeExp checkedArgs == argsT then
          TApp fct checkedArgs returnT
        else error' f $ "Type error in function call '" ++ fct
             ++ "': arguments types do not match function type. Expected \n    "
             ++ show argsT ++ "\ngot " ++ show (map typeExp checkedArgs)
    
  typeCheckE Joker _ _ _ = TJoker
          
instance TypeableExp AExp TAExp where
  typeCheckE (CstInt x) _ _ _ = TCstInt x
  typeCheckE (CstFloat x) _ _ _ = TCstFloat x
  typeCheckE aexp f env fun =
    let ch = \e1 e2 -> check e1 e2 f env fun [TypInt, TypFloat []] eqType in
    case aexp of
      Plus e1 e2 -> ch e1 e2 TPlus
      Minus e1 e2 -> ch e1 e2 TMinus
      Times e1 e2 -> ch e1 e2 TTimes
      Div e1 e2 -> ch e1 e2 TDiv
      Mod e1 e2 -> ch e1 e2 TMod
  
instance TypeableExp BExp TBExp where
  typeCheckE CstTrue _ _ _ = TCstTrue
  typeCheckE CstFalse _ _ _ = TCstFalse
  typeCheckE (And e1 e2) f env fun = 
    check e1 e2 f env fun [TypBool] eqType TAnd
  typeCheckE (Or e1 e2) f env fun =
    check e1 e2 f env fun [TypBool] eqType TOr
  typeCheckE (Not e) f env fun = 
    if typeExp e' == TypBool then TNot e'
    else error' f $ "Type error, expression's type must be Bool, got "
         ++ (show $ typeExp e') ++ "."
    where e' = typeCheckE e f env fun
  typeCheckE bexp f env fun =
    let ch = \e1 e2 -> check e1 e2 f env fun [TypBool, TypInt, TypFloat []] eqType in
    let ch' = \e1 e2 -> check e1 e2 f env fun [TypInt, TypFloat []] eqType in
    case bexp of
      Equal e1 e2 -> ch e1 e2 TEqual
      Inequal e1 e2 -> ch e1 e2 TInequal
      Lower e1 e2 -> ch' e1 e2 TLower
      LowerEq e1 e2 -> ch' e1 e2 TLowerEq
      Greater e1 e2 -> ch' e1 e2 TGreater
      GreaterEq e1 e2 -> ch' e1 e2 TGreaterEq