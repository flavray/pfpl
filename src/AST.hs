-- | This module defines the two (typed/untyped) ASTs
module AST where

import Data.List
import Data.Int (Int32)

-- | Untyped AST

data Ast = 
  Function String [(String, Type)] Type Command
  | StructDef String [(String, Type)]
  | SSeq Ast Ast
  deriving (Show, Eq)
           
-- | Typed AST

data TAst = 
  TFunction String [(String, Type)] Type TCommand
  | TStructDef String [(String, Type)] 
  | TSSeq TAst TAst
  deriving (Show, Eq)

-- | Untyped commands

data Command = 
  Decl String Exp
  | ConstDecl String Exp
  | Affect Exp Exp
  | If BExp Command (Maybe Command)
  | While BExp Command
  | CaseOf Exp [(Exp, Command)]
  | Print Exp
  | Seq Command Command
  | Return (Maybe Exp)
  deriving (Show, Eq)
           
-- | Typed commands

data TCommand = 
  TDecl String TExp
  | TConstDecl String TExp
  | TAffect TExp TExp
  | TArrayAffect String String TExp Int Type
  | TIf TBExp TCommand (Maybe TCommand)
  | TWhile TBExp TCommand
  | TCaseOf TExp [(TExp, TCommand)]
  | TPrint TExp
  | TSeq TCommand TCommand
  | TReturn (Maybe TExp)
  deriving (Show, Eq)

-- | Untyped expressions

data Exp = 
  Var String
  | ConstVar String
  | ArrayElement String [Exp]
  | Array [Exp] Type
  | Cast Exp
  | Read Type
  | A AExp
  | B BExp
  | App String [Exp]
  | Struct [(String, Exp)] Type
  | Joker
  deriving (Show, Eq)

-- | Typed expressions

data TExp = 
  TVar String Type
  | TConstVar String Type
  | TArrayElement String [TExp] Type
  | TArray [TExp] Type
  | TCast TExp Type
  | TRead Type
  | TA TAExp Type
  | TB TBExp Type
  | TApp String [TExp] Type
  | TStruct [(String, TExp)] Type
  | TJoker
  deriving (Show, Eq)
  
-- | Untyped Core

data AExp = 
  CstInt Int32
  | CstFloat Float
  | Plus Exp Exp
  | Minus Exp Exp
  | Times Exp Exp
  | Div Exp Exp
  | Mod Exp Exp
  deriving (Show, Eq)
            
data BExp = 
  CstTrue
  | CstFalse
  | And Exp Exp
  | Or Exp Exp
  | Not Exp
  | Equal Exp Exp
  | Inequal Exp Exp
  | Lower Exp Exp
  | LowerEq Exp Exp
  | Greater Exp Exp
  | GreaterEq Exp Exp
  deriving (Show, Eq)
           
-- | Typed Core

data TAExp = 
  TCstInt Int32
  | TCstFloat Float
  | TPlus TExp TExp
  | TMinus TExp TExp
  | TTimes TExp TExp
  | TDiv TExp TExp
  | TMod TExp TExp
  deriving (Show, Eq)
            
data TBExp = 
  TCstTrue
  | TCstFalse
  | TAnd TExp TExp
  | TOr TExp TExp
  | TNot TExp
  | TEqual TExp TExp
  | TInequal TExp TExp
  | TLower TExp TExp
  | TLowerEq TExp TExp
  | TGreater TExp TExp
  | TGreaterEq TExp TExp
  deriving (Show, Eq)
                   
-- | Types

data Type = 
  TypInt 
  | TypFloat Dimension
  | TypBool
  | TypVoid
  | TypVar String -- Type variable, not a variable itself.
  | TypFun [Type] Type
  | TypArray Int Type
  | TypJoker
           
-- | Dimensions

type Dimension = [(String, Int32)]
           
-- | Corrects types for function return

isReturnType t = case t of
  TypInt -> True  
  TypFloat _ -> True
  TypBool -> True 
  TypVoid -> True
  _ -> False
                   
instance (Show Type) where 
  show TypInt = "Int"
  show (TypFloat dim) = 
    "Float ~ " ++ case dim of 
      [] -> "1"
      _ -> concat $ intersperse "." (map (\(d, i) -> d ++ "^" ++ show i) dim)
  show TypBool = "Bool"
  show TypVoid = "Void"
  show (TypVar t) = t
  show TypJoker = "_"
  show (TypFun argsT returnT) = 
    "[" ++ concat (intersperse ", " (map show argsT)) ++ "] -> " ++ show returnT
  show (TypArray dim t) = show t ++ "_Array[" ++ show dim ++ "]"
  
instance (Eq Type) where
  TypFloat _ == TypFloat _ = True
  TypInt == TypInt = True
  TypBool == TypBool = True
  TypVoid == TypVoid = True
  TypVar v == TypVar w = v == w
  TypFun l r == TypFun l2 r2 = l == l2 && r == r2
  TypArray i t == TypArray i2 t2 = i == i2 && t == t2
  _ == _ = False