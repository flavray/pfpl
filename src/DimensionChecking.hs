import Data.List

import Checker
import AST
{-import Parser
import Lexer
-}
  

-- | Groups together units of same name. The input list has to be sorted 
-- relatively to the units name.
pack [] acc curr = reverse $ curr:acc 
pack ((u, e):xs) acc curr@(uCurr, eCurr) =
  if u == uCurr 
  then pack xs acc (uCurr, eCurr + e)
  else pack xs (curr:acc) (u, e)

-- | Normalize a dimension to get a unique form
normalize :: Dimension -> Dimension
normalize [] = []
normalize l = srt (abs . snd) . filter (\a -> snd a /= 0) $ pack (tail s) [] (head s)
  where srt f = sortBy (\a b -> compare (f a) (f b))
        s = srt fst l
            
test = normalize [("m", 2), ("s", -2), ("m", 2)]